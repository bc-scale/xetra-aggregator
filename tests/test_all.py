"""
Testing suite

#@pytest.mark.parametrize('num1, num2, expected',[(3,5,8), (-2,-2,-4), (-1,5,4), (3,-5,-2), (0,5,5)])
#def test_sum(num1, num2, expected):
#        assert sum(num1, num2) == expected
"""

import requests


def test_hello():
    """
    Tests the root node if its working as expected
    """

    result = requests.get("http://localhost:8080")

    compare = {
        "code" : 200,
        "text" : '{"Hello":"World"}'
    }

    assert result.status_code == compare['code'], f"Expected http code {compare['code']}, got: {result.status_code}"
    assert result.text == compare['text'], f"Expected {compare['text']} from root, got: {result.text}"
